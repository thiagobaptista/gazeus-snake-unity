﻿using UnityEngine;
using System.Collections;

public interface IUnityService
{
    float GetInputFromAxis(string axisName);
}

public class UnityService : IUnityService
{
    public float GetInputFromAxis(string axisName)
    {
        return Input.GetAxis(axisName);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDDGameController : MonoBehaviour
{
    #region Attrtibutes and Properties
    public TDDSnake Snake
    {
        get
        {
            return new TDDSnake();
        }
    }

    Vector3 nextMovement = Vector3.right;
    public Vector3 NextMovement
    {
        get
        {
            return nextMovement;
        }
    }

    IMovementCalculator movementCalculator;
    public IMovementCalculator MovementCalculator
    {
        set
        {
            movementCalculator = value;
        }
    }

    public UnityService UnityService
    {
        set
        {
            //
        }
    }
    #endregion

    #region Unity API
    void Update()
    {
        CalculateNextMovement();
    }

    private void CalculateNextMovement()
    {
        nextMovement = movementCalculator.Calculate(0, 0, Vector3.right);
    }
    #endregion
}

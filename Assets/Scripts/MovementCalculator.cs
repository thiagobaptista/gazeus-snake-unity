﻿using System;
using UnityEngine;

public interface IMovementCalculator
{
    Vector3 Calculate(float axisHorizontal, float axisVertical, Vector3 previousMovement);
}

public class MovementCalculator : IMovementCalculator
{
    public Vector3 Calculate(float axisHorizontal, float axisVertical, Vector3 previousMovement)
    {
        if (axisHorizontal > 0 && previousMovement != Vector3.left) return Vector3.right;
        if (axisHorizontal < 0 && previousMovement != Vector3.right) return Vector3.left;
        if (axisVertical > 0 && previousMovement != Vector3.down) return Vector3.up;
        if (axisVertical < 0 && previousMovement != Vector3.up) return Vector3.down;

        return previousMovement;
    }
}
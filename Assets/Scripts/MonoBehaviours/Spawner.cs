﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject foodObject;

    public void Spawn(
        int upperLimit,
        int lowerLimit,
        int leftLimit,
        int rightLimit,
        List<Vector3> snakePiecesPositions)
    {
        Vector3 position = GetPosition(upperLimit, lowerLimit, 
            leftLimit, rightLimit, snakePiecesPositions);

        Instantiate(foodObject, position, Quaternion.identity);
    }

    private Vector3 GetPosition(
        int upperLimit,
        int lowerLimit,
        int leftLimit,
        int rightLimit,
        List<Vector3> snakePiecesPositions)
    {
        int x = Random.Range(leftLimit, rightLimit);
        int y = Random.Range(lowerLimit, upperLimit);
        Vector3 position = new Vector3(x, y, 0);

        if (snakePiecesPositions.Contains(position))
        {
            return GetPosition(upperLimit, lowerLimit, 
                leftLimit, rightLimit, snakePiecesPositions);
        }
        return position;
    }
}

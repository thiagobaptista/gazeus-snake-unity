﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
    public GameObject tailPrefab;

    ISnakeListener listener;
    public ISnakeListener Listener
    {
        set
        {
            listener = value;
        }
    }

    List<Transform> tail = new List<Transform>();

    bool ate = false;

    public void Move(Vector3 nextPosition)
    {
        Vector3 lastPosition = transform.position;
        transform.position += nextPosition;

        if (ate)
        {
            ate = false;

            GameObject tailPiece = Instantiate(tailPrefab, lastPosition, Quaternion.identity);
            tail.Insert(0, tailPiece.transform);
        }
        else if (tail.Count > 0)
        {
            Transform last = tail[tail.Count - 1];
            last.transform.position = lastPosition;

            tail.Insert(0, last);
            tail.RemoveAt(tail.Count - 1);
        }
    }

    public List<Vector3> GetPiecesPositions()
    {
        List<Vector3> positions = new List<Vector3>();

        positions.Add(transform.position);
        foreach (Transform t in tail)
        {
            positions.Add(t.position);
        }

        return positions;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Food food = other.GetComponent<Food>();
        if (food != null)
        {
            ate = true;
            Destroy(other.gameObject);
            if (listener != null)
            {
                listener.OnEat();
            }
        }

        if (other.tag == "Tail")
        {
            if (listener != null)
            {
                listener.OnHit();
            }
        }

        if (other.tag == "Border")
        {
            if (listener != null)
            {
                listener.OnHit();
            }
        }
    }
}

public interface ISnakeListener
{
    void OnHit();

    void OnEat();
}

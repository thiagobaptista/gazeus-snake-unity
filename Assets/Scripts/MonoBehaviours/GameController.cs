﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour, ISnakeListener
{
    #region Public Variables
    [Header("Snake config")]
    public Snake snake;

    public float updateInterval = .5f;

    [Header("Spawner config")]
    public Spawner spawner;

    [Header("UI config")]
    public GameObject gameOverScreen;

    public Text scoreText;
    #endregion

    #region Private Attributes
    IMovementCalculator movementCalculator = new MovementCalculator();

    IUnityService service = new UnityService();

    int upperLimit = 4, 
        lowerLimit = -4, 
        leftLimit = -7, 
        rightLimit = 7;

    Vector3 nextMovement;

    int score;
    #endregion

    #region Unity API
    void Start()
    {
        nextMovement = new Vector3(1, 0, 0);

        snake.Listener = this;

        SpawnFood();
        
        InvokeRepeating("MoveSnake", 0, updateInterval);
    }

    void Update()
    {
        SetupNextMovement();
    }
    #endregion

    #region Controller Methods
    void MoveSnake()
    {
        snake.Move(nextMovement);
    }

    void SetupNextMovement()
    {
        nextMovement = movementCalculator
            .Calculate(service.GetInputFromAxis("Horizontal"),
            service.GetInputFromAxis("Vertical"),
            nextMovement);
    }

    public void Restart()
    {
        SceneManager.LoadScene("Main");
    }

    public void OnHit()
    {
        CancelInvoke("MoveSnake");
        GameOver();
    }

    public void OnEat()
    {
        SpawnFood();
        score++;
    }

    void SpawnFood()
    {
        spawner.Spawn(upperLimit, lowerLimit, 
            leftLimit, rightLimit, snake.GetPiecesPositions());
    }

    void GameOver()
    {
        scoreText.text = "score: " + score;
        gameOverScreen.SetActive(true);
    }
    #endregion
}

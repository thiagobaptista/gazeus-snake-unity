﻿using NUnit.Framework;

public class UnityServiceTest
{
    [Test]
    public void Should_get_horizontal_input_axis_from_string()
    {
        float desiredInput1 = 1;
        IUnityService service = new MockUnityService(desiredInput1, 0);
        
        Assert.AreEqual(desiredInput1, service.GetInputFromAxis("Horizontal"));

        float desiredInput2 = -0.3f;
        service = new MockUnityService(0, desiredInput2);

        Assert.AreEqual(desiredInput2, service.GetInputFromAxis("Vertical"));
    }
}

public class MockUnityService : IUnityService
{
    float desiredHorizontalAxis, desiredVerticalAxis;

    public MockUnityService(float desiredHorizontalAxis, float desiredVerticalAxis)
    {
        this.desiredHorizontalAxis = desiredHorizontalAxis;
        this.desiredVerticalAxis = desiredVerticalAxis;
    }

    public float GetInputFromAxis(string axisName)
    {
        if (axisName == "Horizontal")
        {
            return desiredHorizontalAxis;
        }
        else if (axisName == "Vertical")
        {
            return desiredVerticalAxis;
        }
        return 0;
    }
}
﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class GameControllerTest
{
    private GameObject controllerObject;

    private TDDGameController controller;

    [SetUp]
    public void Setup()
    {
        controllerObject = new GameObject();
        controller = controllerObject.AddComponent<TDDGameController>();
    }

    [TearDown]
    public void TearDown()
    {
        Object.DestroyImmediate(controller);
        Object.DestroyImmediate(controllerObject);
    }

    [Test]
    public void Should_have_a_Snake_object()
    {
        TDDSnake snake = controller.Snake;

        Assert.NotNull(snake);
    }

    [Test]
    public void Should_have_a_Next_Movement_parametert()
    {
        Vector3 expectedNextMovement = Vector3.right;
        Vector3 nextMovement = controller.NextMovement;

        Assert.AreEqual(expectedNextMovement, nextMovement);
    }

    [UnityTest]
    public IEnumerator Should_the_Next_Move_be_Vector_RIGHT_by_receiving_no_input()
    {
        Vector3 expectedNextMovement = Vector3.right;

        yield return null;

        Assert.AreEqual(expectedNextMovement, controller.NextMovement);
    }

    //[UnityTest]
    public IEnumerator Should_the_Next_Move_be_Vector_UP_by_receiving_input_up_in_horizontal_axis()
    {
        Vector3 expectedNextMovement = Vector3.up;

        IMovementCalculator calculator = new MockMovementCalculator(expectedNextMovement);
        controller.MovementCalculator = calculator;

        yield return null; // Is NOT making the engine call for the controller's update!!

        Assert.AreEqual(expectedNextMovement, controller.NextMovement);
    }

    //[UnityTest]
    // TODO: implement this test
    /*public IEnumerator Should_the_Next_Move_be_set_by_receiving_input()
    {
        Vector3 expectedNextMovement = Vector3.up;

        IMovementCalculator calculator = new MockMovementCalculator(Vector3.up);
        controller.MovementCalculator = calculator;

        IUnityService service = new MockUnityService();
        controller.UnityService = service;
        
        yield return null;

        // TODO poll for change in behaviour

        yield return null;

        Assert.AreEqual(expectedNextMovement, controller.NextMovement);
    }*/
}

public class MockMovementCalculator : IMovementCalculator
{
    Vector3 nextMovement;

    public MockMovementCalculator(Vector3 nextMovement)
    {
        this.nextMovement = nextMovement;
    }

    public Vector3 Calculate(float axisHorizontal, float axisVertical, Vector3 previousMovement)
    {
        return nextMovement;
    }
}

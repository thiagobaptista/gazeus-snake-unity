﻿using UnityEngine;
using NUnit.Framework;

public class MovementCalculatorTest
{
    IMovementCalculator calculator;

    Vector3[] allVectors = new Vector3[] { Vector3.up, Vector3.down, Vector3.left, Vector3.right };
    Vector3[] allVectorsButRight = new Vector3[] { Vector3.up, Vector3.down, Vector3.left };
    Vector3[] allVectorsButLeft = new Vector3[] { Vector3.up, Vector3.down, Vector3.right };
    Vector3[] allVectorsButUp = new Vector3[] { Vector3.down, Vector3.left, Vector3.right };
    Vector3[] allVectorsButDown = new Vector3[] { Vector3.up, Vector3.left, Vector3.right };

    [SetUp]
    public void Setup()
    {
        calculator = new MovementCalculator();
    }

    [Test]
    public void Should_calculate_Vector_RIGHT_from_horizontal_axis_input_1()
    {
        foreach (Vector3 vector in allVectorsButLeft)
        {
            Vector3 previousMovement = vector;
            Vector3 expectedNextMovement = Vector3.right;
            Vector3 nextMovement = calculator.Calculate(1, 0, previousMovement);

            Assert.AreEqual(expectedNextMovement, nextMovement);
        }
    }

    [Test]
    public void Should_not_calculate_Vector_RIGHT_if_previous_movement_was_Vector_LEFT()
    {
        Vector3 previousAndExpectedMovement = Vector3.left;
        Vector3 nextMovement = calculator.Calculate(1, 0, previousAndExpectedMovement);

        Assert.AreEqual(previousAndExpectedMovement, nextMovement);
    }

    [Test]
    public void Should_calculate_Vector_LEFT_from_horizontal_axis_input_minus_1()
    {
        foreach (Vector3 vector in allVectorsButRight)
        {
            Vector3 previousMovement = vector;
            Vector3 expectedNextMovement = Vector3.left;
            Vector3 nextMovement = calculator.Calculate(-1, 0, previousMovement);

            Assert.AreEqual(expectedNextMovement, nextMovement);
        }
    }

    [Test]
    public void Should_calculate_Vector_UP_from_vertical_axis_input_1()
    {
        foreach (Vector3 vector in allVectorsButDown)
        {
            Vector3 previousMovement = vector;
            Vector3 expectedNextMovement = Vector3.up;
            Vector3 nextMovement = calculator.Calculate(0, 1, previousMovement);

            Assert.AreEqual(expectedNextMovement, nextMovement);
        }
    }

    [Test]
    public void Should_calculate_Vector_DOWN_from_vertical_axis_input_minus_1()
    {
        foreach (Vector3 vector in allVectorsButUp)
        {
            Vector3 previousMovement = vector;
            Vector3 expectedNextMovement = Vector3.down;
            Vector3 nextMovement = calculator.Calculate(0, -1, previousMovement);

            Assert.AreEqual(expectedNextMovement, nextMovement);
        }
    }

    [Test]
    public void Should_calculate_previous_movement_if_there_was_no_input_at_all()
    {
        foreach (Vector3 vector in allVectors)
        {
            Vector3 previousAndExpectedMovement = vector;
            Vector3 nextMovement = calculator.Calculate(0, 0, previousAndExpectedMovement);

            Assert.AreEqual(previousAndExpectedMovement, nextMovement);
        }
    }
}